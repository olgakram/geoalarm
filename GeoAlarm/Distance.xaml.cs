﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента пустой страницы см. по адресу http://go.microsoft.com/fwlink/?LinkID=390556

namespace GeoAlarm
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class Distance : Page
    {
        string textRadioButton;
        public Distance()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Вызывается перед отображением этой страницы во фрейме.
        /// </summary>
        /// <param name="e">Данные события, описывающие, каким образом была достигнута эта страница.
        /// Этот параметр обычно используется для настройки страницы.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                if (e.Parameter.ToString() == R1.Content.ToString())
                    R1.IsChecked = true;
                if (e.Parameter.ToString() == R2.Content.ToString())
                    R2.IsChecked = true;
                if (e.Parameter.ToString() == R3.Content.ToString())
                    R3.IsChecked = true;
            }
        }

        //TODO: Оброботчик сохронения выбора
        private void AcceptAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            //1. Переход на предыдущу страницу с передачей параметра
            var stackPage = this.Frame.BackStack;
            int сountBackStackDepth = this.Frame.BackStackDepth;
            var toBackPage = stackPage[сountBackStackDepth - 1];
            this.Frame.Navigate(toBackPage.SourcePageType, textRadioButton);

            //2. Очистка нежелательной истории переходов создаваемой "Navigate". Метод "GoBack" историю очищает, а "Navigate" добавляет.
            сountBackStackDepth = this.Frame.BackStackDepth;
            stackPage.Remove(stackPage[сountBackStackDepth - 1]);
            stackPage.Remove(stackPage[сountBackStackDepth - 2]);
        }

        //TODO: Оброботчик события выбора radio-кнопки
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            textRadioButton = (string)((RadioButton)sender).Content;
        }

        //TODO: Оброботчик отменны выбора
        private void CancelAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }
    }
}
